
let password = document.querySelectorAll('.icon-password');

password.forEach(function (icon) {
    icon.addEventListener('click', function () {
        const target = icon.getAttribute('data-type');
        let inputPassword = document.querySelector(target);
        if (inputPassword.type === 'password') {
            inputPassword.type = 'text';
            icon.classList.replace('fa-eye', 'fa-eye-slash');
        } else {
            inputPassword.type = 'password';
            icon.classList.replace('fa-eye-slash', 'fa-eye');
        }
    });
});

const btnConfirm = document.querySelector('.btn');

btnConfirm.addEventListener('click', (ev) => {
    const password = document.getElementById('password').value;
    const passwordConfirm = document.getElementById('confirm-password').value;

    ev.preventDefault();
    if (password === "" || passwordConfirm === "") {
        alert('Input fields are empty!');
    }
    else if (password === passwordConfirm) {
        alert('You are welcome!');
    }
    else {
        alert('Must enter the same values!');
    }
});

