let ul = document.querySelector('.tabs');
ul.addEventListener('click', function (ev) {
    let data = ev.target.dataset.tab;
    document.querySelector('.paragraphs').classList.remove('paragraphs');
    document.querySelector('.active').classList.remove('active');
    document.querySelector(`[data-li = ${data}]`).classList.add('paragraphs');

    ev.target.classList.add('active');
});
