function showElementsList(arr, parUl = document.body) {
    let ul = document.createElement('ul');
    arr.forEach((element) => {
        let li = document.createElement('li');
        li.append(element);
        ul.append(li);
    });
    parUl.prepend(ul);
}

showElementsList(["1", "2", "3", "sea", "user", 23])