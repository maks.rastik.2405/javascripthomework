function changeTheme() {
    let body = document.getElementsByTagName('body')[0];
    let button = document.getElementsByTagName('button')[0];

    if (body.classList.contains('theme')) {
        body.classList.remove('theme');
        button.innerText = 'Change Theme';
        localStorage.setItem('themes', 'default');
    } else {
        body.classList.add('theme');
        button.innerText = "Return to default theme";
        localStorage.setItem('themes', 'color');
    }
}

let savedTheme = localStorage.getItem('theme');
if (savedTheme === 'color') {
    let body = document.getElementsByTagName('body')[0];
    let button = document.getElementsByTagName("button")[0];
    body.classList.add('theme');
    button.innerText = 'Return to default theme';
}