function createNewUser() {
    name = prompt('Enter you name');
    lastName = prompt('Enter you last name');
    birthDay = prompt('Enter your date of birth, `dd.mm.yyyy`');
    let newUser = new Object();
    newUser.getLogin = function () {
        return `${(name.charAt(0) + lastName).toLowerCase()}`;
    };
    newUser.getAge = function () {
        return `${new Date().getFullYear() - birthDay.slice(6)}`;
    };
    newUser.getPassword = function () {
        return `${name[0].toUpperCase() + lastName.toLowerCase() + birthDay.slice(6)}`;
    };
    return newUser;
}
let user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());

